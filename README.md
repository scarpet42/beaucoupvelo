#  Cet été, j'ai fait beaucoup de vélo

Ce projet est destiné à produire un document LaTeX.

## Le document produit raconte le plus grand trajet que j'ai fait à vélo

C'est le troisième voyage que j'ai raconté. 
Je n'ai pas quitté la France mais c'est le plus grand trajet que je n'avais jamais fait.
Je suis parti de Paris vers l'est, le sud et l'ouest de la France puis suis rentré à Paris.
Ce voyage s'est passé en juin 2006.
Cela a représenté 3700 km en 18,5 jours de vélo.
À l'époque, je dormais systématiquement à l'hôtel.

## Lecture du document compilé
Le document compilé est disponible ici : [beaucoupvelo.pdf](https://scarpet42.gitlab.io/beaucoupvelo/beaucoupvelo.pdf)

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
